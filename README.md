# editorjs-underline

Editorjs inline tool: Text underline

## Install
* yarn add editeditorjs-underline
* npm i editorjs-underline

## Usage
Add a new Tool to the tools property of the Editor.js initial config.

```
const TextUnderline = require("editorjs-underline")
var editor = EditorJS({
  ...
  
  tools: {
    ...
    underline: TextUnderline,
  }
  
  ...
});
```

